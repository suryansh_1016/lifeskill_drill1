# Energy Management
 
### Question 1: What are the activities you do that make you relax - Calm quadrant?

__The activites that keep me relaxed are__

- Listening music
- Nature walks
- sketching
- trekking
- sleeping

---

###  Question 2: When do you find getting into the Stress quadrant?

__Usually i found myself when__

- I have pending work
- approaching deadlines
- I have made some mistake
- I overspend even on tight budget
- I have social Pressures of taking part in social activitis when i dont want to.

---

### Question 3: How do you understand if you are in the Excitement quadrant?

Its really easy to know when I am in excitement quadrant as at those times i am stress free , i enjoy the things i am doing, getting adrenaline rush while i get new experiences. i am not scared to face new chalanges rather i enjoy taking on those challanges.

---

### Question 4: Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

- Sleep is essential for our health and well-being.
- Sleep deprivation weakens the immune system.
- Short sleep duration is linked to numerous health problems, 
including cancer, Alzheimer's disease, and heart disease.
- Sleep deprivation can negatively affect the brain circuits involved in learning and memory.
- Sleep is a biological necessity, not a luxury.
- Keeping the bedroom cool (around 65 degrees Fahrenheit) can improve sleep quality.

---

### Question 5: What are some ideas that you can implement to sleep better?

- making a fixed sleeping routine.
- set reminders to sleep on time.
- avoiding caffiene intake around evening or in general.
- avoid sleeping during the day.
- keep the room cool.
- expose myself to sunlight in the day.

---

### Question 6: Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

- Exercise is the most transformative thing you can do for your brain. It has immediate, long-lasting, and protective benefits.

- Exercise increases the levels of dopamine, serotonin, and noradrenaline, which improves mood, focus, attention, and reaction times.

- Exercise increases the size of the hippocampus, which is responsible for long-term memory.

- Exercise protects the brain from neurodegenerative diseases like Alzheimer's disease and dementia.

- The minimum amount of exercise recommended is 3-4 times a week for 30 minutes each time.

---

### Question 7: What are some steps you can take to exercise more?

- set some goals to keep me motivated like to gain some amount of muscles or lose some weight.

- indulge in the physical activities that I enjoy like swimming, dancing , running , playing football etc.

- start with a small amount of workout then gradually increase it over time like aim 30 mins in start then increase it slowly by time.

- tracking my progress can keep me motivated to continue exercising.

- reward myself with little things when I achieve my goals..







