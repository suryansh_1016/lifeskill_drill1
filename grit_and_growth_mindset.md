# Grit and Growth Mindset

### Q1. Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

 This video talks about the importance of grit, that is passion and perseverance for long term goals. according to Angela Lee Duckworth it is the most significant predictor of success.it is the ability to keep working towards long term goals even in the times of difficulty.


 ### Q2. Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

This video talks about the importance of growth mindset. people with growth mindset believe their abilities can be developed through effort and learning. people with a growth mindset are more likely to embrace challenges, learn from mistakes, and be open to feedback.

### Q3. What is the Internal Locus of Control? What is the key point in the video?

Internal Locus of control is believing you control your life not the external factors.

**KeyPoints of the video**
- the students who were praised for their intelligence developed external locus of control.

-where as the students who were praised for their hardwork developed internal locus of control, and turned out to become more hardworking.  

-video explain having internal locus of control keeps you motivated and put more efforts in the things you do.

### Q4. What are the key points mentioned by speaker to build growth mindset (explanation not needed).

- Trust in your capacity to learn and overcome challenges through dedicated effort.

- Don't settle for assumptions; actively question them and seek diverse perspectives.

- Take charge of your development by actively seeking experiences that promote growth.

- Recognize that challenges, not just successes, offer valuable opportunities to learn and evolve.

### Q5. What are your ideas to take action and build Growth Mindset?

- stay positive even when the situation seems dificult. 

- develop a learning attitude that you can learn new skills if you put effort on it.

- develop internal locus of control.

- trust in your capability to overcome challanges.

