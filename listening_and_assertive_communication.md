# Active Listening

## Q1. What are the steps/strategies to do Active Listening? (Minimum 6 points)

- **Give Your Full Attention:** Avoid getting distracted by your own thoughts.

- Must focus on the speaker and the topic.

- **Avoid Interrupting:** Try not to intrupt the other person between conversation , wait them to finish first.

- **Ask Open-Ended Questions:** Encourage the speaker to elaborate by asking open-ended questions that require more than a yes or no answer.

- **Use door openers** These are phrases that show your are interested and keep the other person talking. 

- **Empathize and Validate:** Acknowledge the speaker's feelings and emotions. Show empathy to connect on a deeper level.

## Q2. According to Fisher's model, what are the key points of Reflective Listening?

### key points of Fisher's model of Reflective Listening.
- **Pay attention** Really listen to what the person is saying, try to understand their feelings and thoughts, and avoid distractions.

- **Reflect what you hear** Briefly restate what they said in your own words to show you understand their message. This doesn't mean agreeing with them, just showing you're listening.

- **Clarify if needed** If something is unclear, ask questions to understand their point better.

- **Focus on feelings** Try to understand and acknowledge the emotions behind their words.

- **Respond, not lead** Don't tell them their problems or solutions, but let them explore their thoughts and feelings openly.

## Q3. What are the obstacles in your listening process?

- **Distractions:** External factors like noise or internal thoughts can hinder listening.

- **Prejudice:** Preconceived notions or biases can affect your ability to listen objectively.

- **Lack of Interest:** If you're not interested in the topic or speaker, your mind may wander.

- **Multitasking:** Trying to do too many things while listening can reduce comprehension.

- **Assumptions:** Assuming you know what the speaker will say may lead to misinterpretation.


## Q4. What can you do to improve your listening?
- **Practice active listening** Pay close attention, summarize key points in your own words, and ask clarifying questions.

- **Minimize distractions** Put away devices, find a quiet environment, and focus solely on the speaker.

- **Show empathy** Acknowledge emotions and try to understand the speaker's perspective.
**Avoid interrupting: Allow the speaker to finish their thoughts before responding.

- **Be patient** Give the speaker time to gather their thoughts and express themselves fully.

- **Embrace diverse communication styles** Be open to understanding different communication styles, including cultural nuances and informal language.
- **Engage in conversation** Ask follow-up questions, share your own experiences, and contribute to the conversation meaningfully.

- **Reflect on your listening** After conversations, reflect on what you heard and what you could improve on for future interactions.

## Q5. When do you switch to Passive communication style in your day to day life?

 - Usually , I switch to this style to end the conversation or to avoid unwanted arguments.

## Q6. When do you switch into Aggressive communication styles in your day to day life?

- I switch into Aggressive communication styles when i feel threatened or disrespected , or in high pressure situations.

## Q7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

- Generally i use Passive Aggressive communication style to crack jokes , or to point out if someone has said something stupid.

## Q8.How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)

### To make my communication assertive, I can do the following things:

- By being respectful towards others opinions and emotions while standing up for on my own.

- By actively listening to others and then offering my thoughts after careful analysis.

- By paying close attention and acknowledging others' concerns.

- By maintaining a confident yet respectful tone while putting up my point.