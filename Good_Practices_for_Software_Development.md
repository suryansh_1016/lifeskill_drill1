# Good Practices for Software Development

### Question 1: Which point(s) were new to you?

__The points that were new to me are following:__

- Use Github gists to share code snippets. Use sandbox environments like Codepen, Codesandbox to share the entire setup.

- Look at the way issues get reported in large open-source projects.

- Sometimes despite all efforts, the requirements are still vague. So during the implementation phase, get frequent feedback so that you are on track and everyone is on the same page

- Make notes while discussing requirements with your team

- The way you ask a question determines whether it will be answered or not. You need to make it very easy for the person to answer your question.

- using app like Boosted to improve productivity.

- Too little food or too much food lead to lower levels of concentration.

---

### Question 2: Which area do you think you need to improve on? What are your ideas to make progress in that area?

__The areas i think i need to improve are__

1. __Communication with my team: most of the times I hesitate to ask questions to my team members when i am stuck. i can imporve this by.__
    - Starting with small questions.
    - Approaching then when they are free.
    - being clear about the problem and the solutions i have tried.
    - Letting my team members know that they can ask questions to in situation like these.

2. __It difficult for me to stay focused while working continuously for long periods, and i can improve it by.__
    - I can break down my task into pieces and set the priority. This will help me work on the important and difficult tasks first so I can do the easy ones later even with low motivation.
    
    - By minimizing distractions: I can sit somewhere quiet, put my phone on silent that way I won't get distracted easily and be more productive

    - Schedule short walks or stretches throughout the day. Physical activity helps improve blood flow and brain function.

    - Look away from your screen every 20 minutes or so for 20 seconds to prevent eye strain.

    - Organize my workspace so I can sit there for long periods.

    - Stay hydrated throughout the day. Dehydration can zap your focus.


