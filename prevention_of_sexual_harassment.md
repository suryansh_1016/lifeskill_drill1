## What kinds of behaviour cause sexual harassment?

### Here are Some main reasons causing sexual harassment:


1. **Unwanted Sexual Advances**: This includes any unwelcome physical contact, such as touching, groping, or kissing, as well as verbal advances like sexual propositions or requests for sexual favors.

2. **Sexual Comments or Jokes**: Making sexually suggestive comments, jokes, or innuendos, whether in person, via electronic communication, or through other means, can create a hostile environment and contribute to sexual harassment.

3. **Sexual Innuendo or Remarks**: Making comments about a person's appearance, body, or sexual characteristics in a manner that makes them uncomfortable or objectifies them can constitute sexual harassment.

4. **Displaying Inappropriate Material**: Sharing or displaying sexually explicit images, videos, or other materials in the workplace or in other contexts where it is unwelcome can contribute to a hostile environment and constitute sexual harassment.

5. **Unwelcome Physical Contact**: Any form of physical contact that is unwanted or inappropriate, including touching, hugging, or other physical gestures, can constitute sexual harassment.

6. **Sexual Propositions or Advances**: Making unwelcome sexual advances, propositions, or requests for sexual favors, especially when tied to promises of preferential treatment or threats of negative consequences, constitutes sexual harassment.

7. **Creating a Hostile Environment**: Behaviors that create a hostile or intimidating environment based on a person's sex or gender, including making derogatory or offensive comments, jokes, or gestures, can constitute sexual harassment.

8. **Persistent or Unwanted Attention**: Continuously pursuing or expressing romantic or sexual interest in someone despite their lack of interest or explicit rejection can constitute sexual harassment.

9. **Retaliation for Rejection**: Retaliating against someone for rejecting or reporting sexual advances or harassment, such as through adverse treatment or threats, is also a form of sexual harassment.


---


## What would you do in case you face or witness any incident or repeated incidents of such behaviour?

### I can do the following things, if I witness or face anything like this:

-  **Speak Up**: I will tell the person it's not okay to makes me or others uncomfortable.

- **Document**: Keep track of what happened, including dates, times, and witnesses.

- **Report**: Inform supervisor or HR, regarding what happened.

- **Follow Up**: Check in with authorities to make sure they're taking action and stopping it from happening again.

-  **Seek Support**: Talk to friends, family, or a counselor if I need help coping.

- **Support Others**: If I see someone else going through it, I'd offer support and encourage them to speak up too.

- **Educate**: Teach others about what's not okay and why it's important to be respectful.




