# Focus Management

### Question 1: What is Deep Work?
deep work is the ability to focus without distraction on a cognitively demanding task.

---

### Question 2: According to author how to do deep work properly, in a few points?

1. Identify your deep work profession: Deep work is best suited for professions that require intense focus on cognitively demanding tasks.
2. Train yourself to focus.
3. Minimize distractions.

### Question 3: How can you implement the principles in your day to day life?

- Identify your deep work tasks
- Schedule time for deep work
- Eliminate distractions to stay focused
- Make it as habit

### Question 4: What are the dangers of social media, in brief?

Social media can be harmful in the following ways.

- It can be addictive and can reduce your ability to concentrate, thus hurting your ability to be productive.
- It can cause feelings of loneliness and inadequacy due to comparing yourself to others' carefully curated online profiles.
- It can create a constant hum of anxiety because of the way our brains react to the intermittent rewards social media provides. ░