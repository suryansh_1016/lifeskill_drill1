# Event Sourcing Architecture

<br>

### Event sourcing is an architectural pattern used in software development where the state of an application is determined by a sequence of events rather than by the current state of the data. In event sourcing, each action or operation performed on the system is captured as an immutable event. These events are persisted in an event log or event store and can be replayed to reconstruct the state of the application at any point in time.


![event.png](https://miro.medium.com/v2/resize:fit:836/1*Ev_PINO729si4s9-JJAKMA.png)


<h2> Here's a detailed explanation of the components and workings of event sourcing architecture:</h2>

<h3>Events :</h3> Events represent discrete occurrences within the system. They capture meaningful state changes or actions performed by users or processes. Each event is a small, immutable record of what happened in the system. Events typically have a unique identifier, a timestamp, and data specific to the event type.

---

<h3>Event Store :</h3>
The event store is a durable storage mechanism where events are persisted. It could be a database, a distributed log, or any other storage system capable of appending new events efficiently. The event store is append-only, meaning events are added to the end of the log but never modified or deleted. This ensures an accurate, immutable record of all past events.

---

<h3>Aggregate Roots : </h3>
In event sourcing, domain entities are often organized around aggregate roots. An aggregate root is an entity that serves as the entry point to a cluster of related domain objects. All operations on the objects within an aggregate are performed through the aggregate root. Aggregate roots encapsulate the business logic and are responsible for handling incoming commands, applying domain rules, and emitting events.

---

<h3>Command Handlers :</h3>   
Command handlers receive incoming commands from clients or other parts of the system. A command represents an intent to perform an action, such as creating a new entity or updating an existing one. Command handlers validate the commands, transform them into events, and pass them to the appropriate aggregate root for processing.

---

<h3>Event Handlers :</h3> Event handlers are responsible for updating the read models or projections based on the events emitted by aggregates. Read models are denormalized views of the data optimized for querying and presenting information to users. Event handlers subscribe to the event stream and update the read models accordingly, ensuring eventual consistency between the read and write sides of the system.

---

<h3>Projection :</h3>
 Projections are derived views of the event data stored in the event store. They represent the current state of the application as reconstructed from the sequence of events. Projections are updated asynchronously in response to new events and are optimized for efficient querying and reporting.

---

<h3>Replayability :</h3> One of the key advantages of event sourcing is its ability to replay events to reconstruct the state of the application at any point in time. This can be useful for debugging, auditing, or rebuilding data in case of corruption or loss. By replaying events from the event store, it's possible to rebuild the state of the application from scratch.

---

 <h3>CQRS (Command Query Responsibility Segregation) :</h3>Event sourcing is often used in conjunction with CQRS, another architectural pattern that separates the responsibilities of handling commands (write side) and queries (read side). CQRS allows for independent scaling, optimization, and maintenance of the write and read sides of the system.

---


<br>
<br>

In summary, the image illustrates the flow of events within an event sourcing architecture, from event generation to persistence, handling, projection, and eventual consistency. It highlights the key components and interactions involved in building event-driven systems based on the event sourcing pattern.


## References :
1. [medium article](https://medium.com/design-microservices-architecture-with-patterns/event-sourcing-pattern-in-microservices-architectures-e72bf0fc9274)

2. [article by Martin Fowler](https://martinfowler.com/eaaDev/EventSourcing.html)