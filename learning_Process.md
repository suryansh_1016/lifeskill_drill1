## Q1.  What is the Feynman Technique? Explain in 1 line.

This is a learning method that involves explaining a topic to an imaginary student to better understand and retain the information.


## Q2. In this video, what was the most interesting story or idea for you?


The thing i find interesting with the Feynman Technique is, I explain things in my head, like I'm teaching someone. No real person needed! This "pretend teaching" helps me understand better by organizing my thoughts and finding any gaps I need to fill in.



## Q3.  What are active and diffused modes of thinking?

### **Active Thinking** Also known as **focused or convergent thinking.**

- requires conscious effort and concerntration.
- This is like studying for a test, concentrating on details, and solving problems step-by-step.

### **Diffused Thinking** Also known as **unfocused or divergent thinking**

- Occurs unconsciously when the mind is relaxed and wanders freely.
- Encourages creativity, innovation, and making connections between seemingly unrelated ideas.
- This is like daydreaming, taking a walk, or letting your mind wander. This mode helps you make connections and sparks creativity.



## Q4. According to the video, what are the steps to take when approaching a new topic? Only mention the points.

- Deconstruct the skill.
- Learn enough to self-correct.
- Remove barriers to learning.
- Practice for at least 20 hours.


## Q5. What are some of the actions you can take going forward to improve your learning process?



**Actions that i can take to improve my learning process**
- Define clear goals.
- Establish a consistent study routine.
- Explore diverse learning resources.
- Take notes for retention and comprehension.
- Practice regularly, especially for skill-based subjects.
- Maintain organized study materials.
- Seek feedback from teachers, mentors, or peers.
- Engage in collaborative learning through group study or projects.
- Stay informed about the latest developments in your field.
- Utilize educational technology wisely.
