# Tiny Habits

### Q1. In this video, what was the most interesting story or idea for you?
The thing I find interesting about the video is that it is not very difficult to make new tiny habits, I just have to decide tiny new habit, that is easy enough to not require high motivation, and follow it up with the existing habits, and celebrate it after I complete it, just by doing it consistently I can form a lot of tiny new habits and change my behavior overall. 

---

 ### Q2. How can you use B = MAP to make making new habits easier? What are M, A and P.
 MAP stands for:
 - **Motivation :** willingness to perform the behavior.
 - **Ability :** Physical and mental capacity to perform the behavior.
 - **Prompt :** trigger that reminds you to perform the behavior.

__we can use B = MAP to make making new habits easier in the following way.__
- Shrink the habit to the tiniest possible version, something that can be done in 30 seconds or less.
- Identify an action prompt, which is something that reminds you to do the habit.
- Celebrate the completion of the habit, no matter how small.

---

### Q3. Why it is important to "Shine" or Celebrate after each successful completion of a habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

Yes!, because this helps to increase your motivation and make it more likely that you will continue to perform the behavior.
--- 

### Q4. In this video, what was the most interesting story or idea for you?
In this video, i find the concept of "aggregation of marginal gains," which means small improvements can lead to big results. 

---

### Q5. What is the book's perspective about Identity?
 the book tells us that our identity is the north star of habit change. This means that changing our identity is the ultimate way to change our habits. The book says that most people work from outcomes to identity, but we should be working from identity to outcomes.
 
### Q6. Write about the book's perspective on how to make a habit easier to do?

__According to the book there are a few ways to make the habit easier to do.__
- Make the cues that trigger your desired habits obvious in your environment.
- Make your desired habits attractive by associating them with positive rewards.
- Make it easy to perform your desired habits by reducing the friction involved.
- Make your desired habits satisfying by associating them with immediate rewards.

---

### Q7. Write about the book's perspective on how to make a habit harder to do?

__According to the book there are few ways to make habit harder to do.__
- Remove the cues that trigger the habit.
- Associate the habit with something negative.
- Increase the friction involved in performing the habit.
- Remove the rewards associated with the habit.

---

### Q8. Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

__To make the cue obvious__
- I can carry a water bottle with me wherever I go.
- set reminders to drink water.

__To make the habit more attractive__
- I can buy a stylish water bottle;
- I can add some slices of fruits and vegetables to make water flavourful and refreshing.

---

### Q9. Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible the process unattractive or hard or the response unsatisfying?

__To make cue invisible__
- I can keep my phone in a bag instead of my pocket.
- I can turn off the notifications of all the unnecessary applications.

__Make the process unattractive__
- delete social media apps.

__Make it dificult__
- removing the applications from the home screen, I waste time on.
- set restrictions on apps to not open when the maximum screen time limit is reached.

